# README

Documentation [here](https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html).

##1 Tables

This is a nice table: 

| Day     | Meal    | Price |
| --------|---------|-------|
| Monday  | pasta   | $6    |
| Tuesday | chicken | $8    |

##2 Another stuff

Here you can put more stuff.
